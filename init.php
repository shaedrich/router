<?php
    use Dotenv\Dotenv;
    use Controllers\CartController;
    use Controllers\ShopController;
    use Controllers\Core\AppController;
    use Controllers\Core\RouteController;
    use Controllers\ProductController;
    use Controllers\OrderController;
    use Models\ProductItem;
    use Doctrine\DBAL\DriverManager;
    use Enums\Environment;

    include 'vendor/autoload.php';
    $dotenv = Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    if ($_ENV['ENVIRONMENT'] === Environment::Development) {
        ini_set('display_errors', '1');
        error_reporting(E_ALL);
    }

    $phpVersion = explode('.', phpversion());
    if (floatval($phpVersion[0].'.'.$phpVersion[1]) < 7.3) {
        throw new \RuntimeException('The code relies on php version 7.4+. You currently use '.phpversion());
    }

    $conn = DriverManager::getConnection([
        'dbname' => $_ENV['DATABASE_NAME'],
        'user' => $_ENV['DATABASE_USER'],
        'password' => $_ENV['DATABASE_PASSWORD'],
        'host' => $_ENV['DATABASE_HOST'],
        'driver' => $_ENV['DATABASE_DRIVER'],
        'path' => $_ENV['DATABASE_PATH'],
        'driverOptions' => [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
        ]
    ]);

    $conn2 = DriverManager::getConnection([
        'dbname' => $_ENV['PROD_DATABASE_NAME'],
        'user' => $_ENV['PROD_DATABASE_USER'],
        'password' => $_ENV['PROD_DATABASE_PASSWORD'],
        'host' => $_ENV['PROD_DATABASE_HOST'],
        'driver' => $_ENV['PROD_DATABASE_DRIVER'],
        'path' => $_ENV['PROD_DATABASE_PATH'],
        'driverOptions' => [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
        ]
    ]);

    include 'database/setup/'.$_ENV['ENVIRONMENT'].'.php';
    //print_r($conn->executeQuery('SELECT name FROM sqlite_master WHERE type="table";')->fetchAllAssociative());

    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_save_path('session');
        session_start();
        echo 'new session at "'.session_save_path().'"'.PHP_EOL;
        $_SESSION['collections'] = [];
    }

    $appController = new AppController();
    $appController->add_dependency(RouteController::class, [ 'shop.index' ]);
    $router = new RouteController('shop.index');
    $router->add((object)[ 'class' => ShopController::class, 'action' => 'index', 'name' => 'shop.index', 'path' => 'shop/index' ]);
    $router->add((object)[ 'class' => ProductController::class, 'action' => 'index', 'name' => 'product.index', 'path' => 'product/index' ]);
    $router->add((object)[ 'class' => CartController::class, 'action' => 'index', 'name' => 'cart', 'path' => 'cart' ]);
    $router->add((object)[ 'class' => CartController::class, 'action' => 'checkout', 'name' => 'cart.checkout', 'path' => 'cart/checkout' ]);
    $router->add((object)[ 'class' => ProductController::class, 'action' => 'detail', 'name' => 'product', 'path' => 'product/{product}', 'params' => [ (object)[ 'is_required' => true, 'name' => 'id' ] ] ]);
    $router->add((object)[ 'class' => CartController::class, 'action' => 'invoice', 'name' => 'invoice', 'path' => 'invoice' ]);
    $router->add((object)[ 'class' => OrderController::class, 'action' => 'detail', 'name' => 'order', 'path' => 'order/{order}', 'httpMethod' => 'GET', 'params' => [ (object)[ 'is_required' => true, 'name' => 'id' ] ] ]);
    $router->add((object)[ 'class' => OrderController::class, 'action' => 'order', 'name' => 'order.place', 'path' => 'order/place', 'httpMethod' => 'POST', 'params' => [
        (object)[ 'is_required' => true, 'name' => 'first_name' ],
        (object)[ 'is_required' => true, 'name' => 'last_name' ],
        (object)[ 'is_required' => true, 'name' => 'street_address' ],
        (object)[ 'is_required' => true, 'name' => 'additional_address_info' ],
        (object)[ 'is_required' => true, 'name' => 'zip_code' ],
        (object)[ 'is_required' => true, 'name' => 'city' ],
        (object)[ 'is_required' => true, 'name' => 'email' ],
        (object)[ 'is_required' => true, 'name' => 'telephone' ]
    ]]);
    $cart = new CartController($router);    
    $cart->add(ProductItem::get($conn, 4));
    $cart->add(ProductItem::get($conn, 5));

    /*$customNamespaces = [ 'Collections', 'Models', 'Controllers' ];
    $classes = array_reduce(get_declared_classes(), function($res, $class) use($customNamespaces) {
        $namespace = explode('\\', $class)[0];
        if (in_array($namespace, $customNamespaces)) {
            if (!array_key_exists($namespace, $res)) {
                $res[$namespace] = [];
            }
            $res[$namespace][] = $class;
        }
        return $res;
    }, []);
    echo '<pre>'.print_r($classes, true).'</pre>';*/