|                 |                |
| --------------- | -------------- |
| **License**     | [MIT](LICENSE) |
| **PHP-Version** | 7.3            |
| **App-Version** | 2.1            |

# Application
The application borrows ideas from the [Laravel framework](https://laravel.com/docs/).

# Contribute
See [Contribute](CONTRIBUTE.md)

# Changelog
See [CHANGELOG](CHANGELOG.md)

# Todo
* Use `Reflection`s for `Route` parameters
* Use `public/` as entry point + .htaccess

[More todos &hellip;](https://dsggit01.dsprodukte.de/haedrich.sebastian/rtv/-/issues)

# Folder structure
```
.
├── classes - class loader entry point
│   ├── Controllers - MVC controllers
│   └── Models - MVC models
├── public - web root
```

# Key features
## Routing
Define routes beforehand in your [init.php](init.php) for `ROUTING_MODE` 'web' and in [api.php](api.php) for `ROUTING_MODE` 'api'.

```php
    // initialize router with base route
    $router = new RouteController('shop.index');
    // add n routes
    $router->add((object)[
        'class' => ShopController::class,
        'action' => 'index',
        'name' => 'shop.index',
        'path' => 'shop/index'
    ]);
    // Display route's contents
    echo $router->navigate();
```

## AppController
The `AppController` handles layout and dependency injection.

### Layout
Layouts are views in the sub folder `views/layouts`. The can be served by invoking the `pageLayout()` method
```php
    echo $appController->layoutPage(
        $appController->layoutPage($router->navigate(), 'skeleton', [
            'sidebar' => $cart->cart_button()
        ]), 'app', [ 'home_url' => $router->route('shop.index') ]
    );
```

### Dependency injection service container
The dependency injection pattern is helpful to keep code clean, flexible and less redundant. But the more dependencies you have, it can get out of hand and you have about the same amount of problems &mdash; you just displaced the problem. But the service container manages and injects dependencies half-automatically.

```php    
    $appController = new AppController();
    // add n dependencies
    $appController->add_dependency(RouteController::class, [ 'shop.index' ]);

    // use them later in the code and just inject the AppController
    $appController->get_dependencies([ RouteController::class ])
```