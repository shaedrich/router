<?php
    namespace shaedrich\Router\Controllers;

    trait Routable
    {
        /* Dependency */
        protected $router;

        public function __construct($router)
        {
            $this->router = $router;
        }
    }