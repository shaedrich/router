<?php
    namespace shaedrich\Router\Controllers;

    use shaedrich\Router\Models\Route;
    use shaedrich\MVC\Models\Model;
    use shaedrich\Router\HttpException;

    //use ReflectionClass;

    class RouteController
    {
        private $routes = [];
        private $currentRoute;
        private $baseRoute;
        /** @var string{'web'|'api'} */
        private $mode = 'web';

        const ROUTING_MODES = [ 'web', 'api' ];

        public function __construct(string $baseRoute)
        {
            $this->baseRoute = $baseRoute;
        }

        /**
         * Add a route to the routes array
         * @param object $route
         * @throws shaedrich\Router\HttpException
         */
        public function add(object $route)
        {
            if ($this->pathExists($route->path)) {
                $e = new HttpException('Route already defined!');
                $e->http_response_code(400);
                throw $e;
            }
            $this->routes[$route->path] = new Route($route);
        }

        /**
         * Checks if the route with a specific path was already added to the routes array
         * @param string $path
         * @return bool
         */
        public function pathExists(string $path): bool
        {
            return array_key_exists($path, $this->routes);
        }

        /**
         * Checks if the route was already added to the routes array
         * @param string $name
         * @return bool
         */
        public function exists(string $name): bool
        {
            return $this->find($name) != false;
        }

        /**
         * Returns the route associated with a specific name
         * @param string $name
         * @return Route|bool
         */
        public function find(string $name)
        {
            return current(array_filter($this->routes, function($route) use($name) {
                return $route->name === $name;
            }));
        }

        /**
         * Assign current route if it exists and fullfils all defined conditions
         * @param string $name
         * @throws shaedrich\Router\HttpException
         */
        public function match(string $name)
        {
            //echo 'check route "'.$name.'"';
            if (!$this->exists($name)) {
                $e = new HttpException('Route "'.$name.'" doesn\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }
            $route = $this->find($name);
            if ($this->mode === 'api' && !in_array($route->action, $route->class::API_METHODS)) {
                $e = new HttpException('Route cannot be accessed through api!');
                $e->http_response_code(403);
                throw $e;
            }
            if ($_SERVER['REQUEST_METHOD'] !== $route->httpMethod) {
                $e = new HttpException('HTTP method mismatch. Use '.$route->httpMethod.' instead!');
                $e->http_response_code(405);
                throw $e;
            }
            $this->currentRoute = $route;
        }

        /**
         * Calls route's class method with the given parameters and returns results
         */
        public function navigate()
        {
            // @todo Use reflection as default parameter source
            /*$actionReflection = new \ReflectionMethod($this->currentRoute->class, $this->currentRoute->action);
            foreach($actionReflection->getParameters() as $parameter) {
                echo $parameter->getType()->getName().' '.$parameter->getName();
            }*/
            global $conn;
            $params = [];
            $requestParams = $this->get_params();
            foreach($this->currentRoute->required_params as $param) {
                if (!in_array($param->name, array_keys($requestParams)) && $param->name !== 'model') {
                    $e = new HttpException('Required parameter missing: '.$param->name);
                    $e->http_response_code(400);
                    throw $e;
                }
                if ($param->name === 'model' && is_a($param->model, Model::class, true)) {
                    $params[$param->name] = $param->model::get($conn, $requestParams['id']);
                } else {
                    $params[$param->name] = $requestParams[$param->name];
                }
            }
            return (new $this->currentRoute->class($this))->{$this->currentRoute->action}(...array_values($params));
        }

        /**
         * Return the respective params and bodies for the HTTP request
         */
        public function get_params(): array
        {
            if (in_array($this->currentRoute->httpMethod, [ 'GET', 'DELETE' ])) {
                return $_GET;
            } else if ($this->currentRoute->httpMethod === 'POST') {
                return array_merge($_GET, $_POST);
            } else if ($this->currentRoute->httpMethod === 'PUT') {
                return json_decode(file_get_contents('php://input'), true);
            }
        }

        /**
         * Returns route path for linking
         * @param string $name
         */
        public function route(string $name)
        {
            if (!$this->exists($name)) {
                $e = new HttpException('Route "'.$name.'" doesn\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }
            return '?route='.urlencode($this->find($name)->name);
        }

        /**
         * Sets routing modes to one of `ROUTING_MODES`
         * @param string{'web'|'api'} $mode
         * @throws shaedrich\Router\HttpException
         */
        public function set_mode(string $mode)
        {
            if (!in_array($mode, self::ROUTING_MODES)) {
                $e = new HttpException('Routing mode must be one of these: '.implode(', ', self::ROUTING_MODES));
                $e->http_response_code(400);
                throw $e;
            }
            $this->mode = $mode;
        }
    }