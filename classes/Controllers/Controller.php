<?php
    namespace shaedrich\Router\Controllers;

    use shaedrich\Router\HttpException;

    class Controller
    {
        const DYNAMIC_PROPERTIES = [];

        /**
         * Parses a view from the `views/` folder and returns its contents
         * @param string $view Name of the view (without file extension)
         * @param array $parameters Key-value map of parameters to replace in the view
         * @return string
         */
        protected function view(string $view, array $parameters = []): string
        {
            if (!file_exists('views/'.$view.'.html')) {
                $e = new HttpException('View does\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }
            $contents = file_get_contents('views/'.$view.'.html') ?: '';
            $contents = str_replace(
                $this->mapToPlaceholder($parameters),
                array_values($parameters),
                $contents
            );

            return $contents;
        }

        /**
         * Maps the keys to the placeholder format to replace
         * @param array $parameters
         * @return array
         */
        private function mapToPlaceholder(array $parameters): array {
            return array_map(function(string $parameter) {
                return '{{'.$parameter.'}}';
            }, array_keys($parameters));
        }
    }