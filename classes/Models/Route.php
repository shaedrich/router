<?php
    namespace shaedrich\Router\Models;

    use shaedrich\Router\HttpException;

    /**
     * @property string $class valid class name
     * @property string [$action=index] existing non-stacic method inside given class
     * @property string $name colon separated lowercase identifier (e.g. root.sub)
     * @property string [$httpMethod=GET] valid HTTP method with response body
     * @property string $path valid relative URL (e.g. root/sub)
     */
    class Route {
        private $class;
        private $action;
        private $name;
        private $httpMethod;
        private $path;
        private $params = [];

        const ALLOWED_HTTP_METHODS = [ 'GET', 'POST', 'PUT', 'PATCH', 'DELETE' ];
        const DYNAMIC_PROPERTIES = [ 'required_params', 'optional_params' ];

        public function __construct(object $route)
        {
            if ($this->check($route)) {
                $this->class = $route->class;
                $this->action = $route->action;
                $this->name = $route->name;
                $this->httpMethod = $route->httpMethod;
                $this->path = $route->path;
                $this->params = $route->params;
            }
        }

        public function __get(string $property)
        {
            if (in_array($property, [ 'class', 'action', 'name', 'httpMethod', 'path', 'params' ])) {
                return $this->{$property};
            } else if (in_array($property, self::DYNAMIC_PROPERTIES)) {
                return $this->{$property}();
            }
            $e = new HttpException('Property "'.$property.'" doesn\'t exist or is unaccessible');
            $e->http_response_code(404);
            throw $e;
        }

        /**
         * Filters `$params` property for params with `is_required` flag enabled
         * @return array
         */
        private function required_params(): array
        {
            return array_filter($this->params, function($param) {
                return $param->is_required;
            });
        }

        /**
         * Filters `$params` property for params with `is_required` flag disabled
         * @return array
         */
        private function optional_params()
        {
            return array_filter($this->params, function($param) {
                return !$param->is_required;
            });
        }

        /**
         * Validates incoming attribues and throws exception when validation fails
         * @param object $route
         * @throws shaedrich\Router\HttpException
         */
        private function check(object $route)
        {
            $this->fill($route);

            if (!class_exists($route->class)) {
                $e = new HttpException('Class "'.$route->class.'"  doesn\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }
            if (!method_exists($route->class, $route->action) && !in_array($route->action, $route->class::DYNAMIC_PROPERTIES)) {
                $e = new HttpException('Action method "'.$route->action.'" doesn\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }
            if (!preg_match('/[a-z.]+/', $route->name)) {
                $e = new HttpException('Name is malformed. Only lowercase letters and colons are allowed!');
                $e->http_response_code(400);
                throw $e;
            }
            if (!in_array($route->httpMethod, self::ALLOWED_HTTP_METHODS)) {
                $e = new HttpException('http method has to be one of: '.implode(', ', self::ALLOWED_HTTP_METHODS));
                $e->http_response_code(400);
                throw $e;
            }

            return true;
        }
        
        /**
         * Sets default values for missing properties
         * @param object $route
         */
        private function fill(object $route)
        {           
            if (!property_exists($route, 'httpMethod')) {
                $route->httpMethod = 'GET';
            }

            if (!property_exists($route, 'name')) {
                $route->name = str_replace('/', '.', preg_replace('/\/\{(.*?)\}/g', '', $this->route));
            }

            if (!property_exists($route, 'action')) {
                $route->action = 'index';
            }

            if (!property_exists($route, 'params')) {
                $route->params = [];
            }
        }
    }