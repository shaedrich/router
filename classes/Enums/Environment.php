<?php
    namespace shaedrich\Router\Enums;

    abstract class Environment /*extends \SplEnum*/ {
        const __default = self::Development;
        
        const Development = 'development';
        const Production = 'production';
    }